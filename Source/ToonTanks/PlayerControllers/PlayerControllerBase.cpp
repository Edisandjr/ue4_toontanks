// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerControllerBase.h"
void APlayerControllerBase::SetPlayerEnabledState(bool SetPlayerEnabled)
{
	if (SetPlayerEnabled)
	{
		GetPawn()->EnableInput(this);
		UE_LOG(LogTemp, Warning, TEXT("Controls Enabled")); 
	}
	else
	{
		GetPawn()->DisableInput(this);
		
		UE_LOG(LogTemp, Warning, TEXT("Controls Disabled"));
	}
	APlayerControllerBase::bShowMouseCursor = SetPlayerEnabled;
}